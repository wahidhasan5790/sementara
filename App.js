import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import ItemInputModal from './src/components/ItemInputModal';

import { formatRupiah } from './src/utils/GeneralFunctions';
import useAppTheme from './Themes';
import axios from './src/apis/axios';

export default function App(props) {

  const theme = useAppTheme();
  const inputModalRef = useRef();

  let [data, setData] = useState([]);

  const loadData = useCallback(() => {
    axios({
      url: 'itemsInCart'
    })
    .then(({ data }) => {
      setData(data);
    });
  }, []);
  const addItemData = useCallback((newItem) => {
    axios({
      url: 'itemsInCart',
      method: 'post',
      data: newItem
    })
    .then(result => {
      loadData();
    })
  }, []);
  const editItemData = useCallback((newItemData) => {
    let {
      id,
      ...newItemDataWithoutId
    } = newItemData;

    axios({
      url: `itemsInCart/${id}`,
      method: 'put',
      data: newItemDataWithoutId
    })
    .then(result => {
      loadData();
    })
  }, []);
  const deleteItemData = useCallback((id = 0) => {
    axios({
      url: `itemsInCart/${id}`,
      method: 'delete'
    })
    .then(result => {
      loadData();
    })
  }, []);

  useEffect(() => {
    loadData();
  }, []);

  const renderItem = useCallback(({item, index}) => (
    <ItemInCart
      {...item}
      editItemData={editItemData}
      deleteItemData={deleteItemData}
    />
  ), []);

  return(
    <SafeAreaProvider>
      <SafeAreaView style={{flex: 1}}>
        <View style={{padding: 10, flex: 1}}>
          <StatusBar barStyle='dark-content' />
          <Text style={{fontSize: 20, marginLeft: 10}}>Shopping Cart</Text>
          <View
            style={{backgroundColor: theme.colors.gray, padding: 10, flex: 1}}
          >
            <FlatList
              data={data}
              style={{flex: 1}}
              keyExtractor={(item, index) => `item-${index}`}
              renderItem={renderItem}
              ItemSeparatorComponent={ListSeparator}
              ListEmptyComponent={EmptyComponent}
              contentContainerStyle={{flex: 1}}
            />
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text>Dummy Product</Text>
              <Button
                title=""
                buttonStyle={{backgroundColor: theme.colors.blue}}
                icon={{
                  type: 'entypo',
                  name: 'plus',
                  size: 15,
                  color: "white"
                }}
                onPress={() => {
                  inputModalRef.current.promptInputForm()
                  .then(newItem => {
                    addItemData(newItem);
                  })
                  .catch(() => {});
                }}
              />
            </View>
          </View>
          <CartResume data={data} />
        </View>
        <ItemInputModal ref={inputModalRef} />
      </SafeAreaView>
    </SafeAreaProvider>
  );
}

function ItemInCart(props) {

  let {
    id = 0, name = '', price = 0, quantity = 1,
    editItemData = () => {},
    deleteItemData = () => {}
  } = props;

  const theme = useAppTheme();

  useEffect(() => {
    if(quantity == 0) {
      deleteItemData(id);
    }
  }, [quantity]);

  return(
    <View
      style={{flexDirection: 'row', padding: 10, backgroundColor: 'white'}}
    >
      <View
        style={{width: 50, height: 50, backgroundColor: 'gray'}}
      />
      <View
        style={{flex: 1, alignItems: 'flex-start', justifyContent: 'space-between', paddingLeft: 10}}
      >
        <Text numberOfLines={2} style={{fontSize: 16, marginBottom: 5}}>{ name }</Text>
        <View style={{backgroundColor: theme.colors.green, paddingHorizontal: 15, paddingVertical: 5, borderRadius: 4}}>
          <Text style={{color: theme.colors.white, fontSize: 12}}>Rp. { formatRupiah(price) }</Text>
        </View>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Button
          title=""
          buttonStyle={{backgroundColor: theme.colors.blue}}
          icon={{
            type: 'entypo',
            name: 'plus',
            size: 15,
            color: theme.colors.white
          }}
          onPress={() => {
            editItemData({
              id,
              name,
              price,
              quantity: parseInt(quantity) + 1
            });
          }}
        />
        <Text style={{marginHorizontal: 5}}>{ quantity }</Text>
        <Button
          title=""
          buttonStyle={{backgroundColor: theme.colors.red}}
          icon={{
            type: 'entypo',
            name: 'minus',
            size: 15,
            color: "white"
          }}
          onPress={() => {
            editItemData({
              id,
              name,
              price,
              quantity: parseInt(quantity) - 1
            });
          }}
        />
      </View>
    </View>
  );
}

function ListSeparator(props) {
  return <View style={{backgroundColor: 'transparent', height: 10}} />
}

function CartResume(props) {
  let { data = [] } = props;

  let [totalQuantity, setTotalQuantity] = useState(0);
  let [totalPrice, setTotalPrice] = useState(0);

  const theme = useAppTheme();

  useEffect(() => {
    let totalP = 0;
    let totalQ = 0;

    for(let item of data) {
      let { quantity = 0, price = 0 } = item;
      totalQ += quantity;
      totalP += (price * quantity);
    }

    setTotalQuantity(totalQ);
    setTotalPrice(totalP);
  }, [JSON.stringify(data)]);

  return(
    <View style={{paddingTop: 30, paddingHorizontal: 30}}>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10}}>
          <Icon
            type='entypo'
            name='shopping-cart'
            size={18}
            style={{marginRight: 5}}
          />
          <Text>{ totalQuantity }</Text>
        </View>
        <Text>Total - Rp { formatRupiah(totalPrice) }</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Button
          title='Close'
          containerStyle={{flex: 2}}
          buttonStyle={{backgroundColor: theme.colors.red}}
        />
        <Button
          title='Go to checkout'
          containerStyle={{flex: 3}}
          buttonStyle={{backgroundColor: theme.colors.blue}}
        />
      </View>
    </View>
  );
}

function EmptyComponent(props) {
  return(
    <View style={{flex: 1, justifyContent: 'center', padding: 20, alignItems: 'center'}}>
      <Text>No data!</Text>
    </View>
  );
}