import React, { useState, useEffect } from 'react';

export const Clean = {
    colors: {
        gray: '#d0d0d0',
        green: '#40a53f',
        white: 'white',
        blue: '#42b4f6',
        red: '#ee2a29'
    }
}

export default function useAppTheme(themeName = 'clean') {

    let [theme, setTheme] = useState(Clean);

    useEffect(() => {
        switch(themeName) {
            default:
                setTheme(Clean);
        }
    }, [themeName]);
    
    return theme;
}