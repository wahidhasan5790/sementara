module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        "root": ["./"], //not used at all, i've ever defined directories here, they are supposed to be root, but not working
        "alias": {
          "apis": "./src/apis",
          "components": "./src/components",
          "utils": "./src/utils"
        }
      }
    ]
  ]
};
