import axios from 'axios';
import {decode, encode} from 'base-64';
import Toast from 'react-native-toast-message';

import * as APIEndpoints from 'constants/APIEndpoints';
import { store } from 'rdx/store';
import * as RootNavigator from 'navigation/RootNavigation';
import {
  APP
} from 'rdx/actionNames';

if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

const appAxios = axios.create({
  baseURL: APIEndpoints.NOTIF_BASE_URL_v1,
  timeout: 50000
});

appAxios.interceptors.request.use(function (config) {
  // Do something before request is sent
  let {
    app: {
      auth: { access_token: accessToken, token_type: tokenType } = {}
    }
  } = store.getState();

  if(tokenType && accessToken) {
    config.headers.common['Authorization'] = `${tokenType} ${accessToken}`;
  }
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

appAxios.interceptors.response.use(
  function(response){
    return response;
  },
  async function(error){
    let { request, response, config } = error;
    if(request.status == 401) {
      try {
        let {
          app: { auth }
        } = store.getState();

        if(auth?.refresh_token != null) {
          let refreshTokenResponse = await axios({
            url: APIEndpoints.BASE_URL + APIEndpoints.Login,
            method: 'post',
            data: {
              grant_type: 'refresh_token',
              refresh_token: auth.refresh_token
            },
            auth: {
              username: 'simple-off-client',
              password: 'simple-off-secret'
            },
            transformRequest: (data) => {
              let formData = new FormData();
              for(let dataKey in data) {
                formData.append(dataKey, data[dataKey]);
              }
              return formData;
            }
          });

          await store.dispatch({
            type: APP.update_auth,
            data: {
              ...auth,
              ...refreshTokenResponse.data
            }
          });

          return appAxios({
            url: config.url,
            method: config.method,
            data: config.data
          });
        }
      } catch(e){
        let { request, response } = e;
        if(request?.status == 500) {
          RootNavigator.reset({index: 0, routes: [{name: 'Login'}]})
          store.dispatch({type: 'RESET_REDUCERS'});
        }
      }
    } else if(response) {
      let { status: responseStatus, data: { errors = [] } } = response;
      errors = errors || [];
      if((responseStatus == 400 || responseStatus == 409) && !config?.skipErrorDialog) {
        if(errors.length == 0) {
          Toast.show({
            type: 'info',
            text1: 'A network error occured, please try again',
            autoHide: true
          });
        } else {
          let errorMessage = errors.join(', ');
          Toast.show({
            type: 'error',
            text1: errorMessage,
            autoHide: false,
            props: {
              button: {
                text: "OK"
              }
            }
          });
        }
      }
    }

    throw error;
  }
);

export default appAxios;
