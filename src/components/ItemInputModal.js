import React, { useState, useCallback, useRef, forwardRef, useImperativeHandle } from 'react';
import { Text, Modal, TouchableOpacity, View } from 'react-native';
import { Button, Input } from 'react-native-elements';

import useAppTheme from '../../Themes';
import { formatRupiah } from '../utils/GeneralFunctions';

export default forwardRef((props, ref) => {

    const [visible, setVisible] = useState(false);
    let [name, setName] = useState('');
    let [price, setPrice] = useState('');

    const theme = useAppTheme();
    const promiseResultRef = useRef();

    const onRequestClose = useCallback(() => {
        setVisible(false);
    }, []);
    const onDismiss = useCallback(() => {
        promiseResultRef.current = null;
        setName('');
        setPrice(0);
    }, []);

    useImperativeHandle(ref, () => ({
        promptInputForm: () => {
            setVisible(true);
            return new Promise((resolve, reject) => {
                promiseResultRef.current = {
                    resolve,
                    reject
                }
            });
        }
    }));

    return(
        <Modal
            visible={visible}
            onRequestClose={onRequestClose}
            onDismiss={onDismiss}
            transparent
            animationType='fade'
        >
            <TouchableOpacity
                activeOpacity={1}
                style={{backgroundColor: 'rgba(0,0,0,0.7)', flex: 1, justifyContent: 'center', padding: 20}}
                onPress={onRequestClose}
            >
                <View style={{padding: 10, backgroundColor: 'white', borderRadius: 10}}>
                    <Text style={{fontSize: 16, marginVertical: 10, alignSelf: 'center'}}>ADD NEW ITEM</Text>
                    <Input
                        label='Product name'
                        onChangeText={text => setName(text)}
                    />
                    <Input
                        label='Product price'
                        inputStyle={{textAlign: 'right'}}
                        value={formatRupiah(price)}
                        onChangeText={text => {
                            let unformattedText = text.replace(/[\.,]/g, '');
                            setPrice(unformattedText);
                        }}
                    />
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                        <Button
                            title='Cancel'
                            containerStyle={{marginRight: 5}}
                            buttonStyle={{backgroundColor: theme.colors.red}}
                            onPress={onRequestClose}
                        />
                        <Button
                            title='Add'
                            buttonStyle={{backgroundColor: theme.colors.blue}}
                            onPress={() => {
                                promiseResultRef.current?.resolve({
                                    name: name,
                                    price: price.replace(/[\.,]/g, ''),
                                    quantity: 1
                                });
                                setVisible(false);
                            }}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </Modal>
    );
});