export const formatRupiah = (amount = 0, decimalCount = 0, decimal = ".", thousands = ",") => {
    try {
      if( amount % 1 != 0 ){
        amount = Number(parseFloat(amount).toFixed(2));
        let withDecimalValue = amount.toString().split('.');
        let mainValue = parseInt(withDecimalValue[0]);
        let decimalValue = !isNaN(withDecimalValue[1]) ? parseInt(withDecimalValue[1]).toString() : '';
        return formatRupiah(mainValue) + '.' + decimalValue;
      } else {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
        const negativeSign = amount < 0 ? "-" : "";
        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;
        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
      }
    }
    catch (e) {
      console.log(e);
    }
  }